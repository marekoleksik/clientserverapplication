import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class Server {
    private static final String version = "1.0.0";
    private static LocalDateTime startTime = LocalDateTime.now();
    private static ServerSocket serverSocket;
    private static Socket socket;
    private static BufferedReader bufferedReader;
    private static PrintWriter printWriter;
    private static String clientName = "client";
    private static Map<String, User> users = new HashMap<>();
    private static String jsonUsersFile = "users.json";

    public static void main(String[] args) throws IOException, ParseException {
        serverSocket = new ServerSocket(4999);
        socket = serverSocket.accept();
        System.out.println("client connected");
        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        printWriter = new PrintWriter(socket.getOutputStream(), true);

        String str;
        while ((str = bufferedReader.readLine()) != null) {

            System.out.println(clientName + ": " + str);
            Message fromClient = new Message(str, clientName);
            fromClient.saveToFile();
            switch (str) {
                case "user":
                    addUser();
                    break;
                case "changePass":
                    changePass();
                    break;
                case "login":
                    login();
                    break;
                case "info":
                    infoServer();
                    break;
                case "uptime":
                    uptimeServer();
                    break;
                case "help":
                    help();
                    break;
                case "stop":
                    stop();
                    break;
                case "badSyntax":
                    sendToClient("Nieprawidlowe polecenie. Wpisz polecenie help, aby zobaczyc dostepne polecenia.");
                    break;
            }
        }
    }

    private static void changePass() throws IOException {
        String newPassword = bufferedReader.readLine();

        if (clientName.equals("client")) {
            sendToClient("musisz byc zalogowany, aby zmienic haslo");
        } else {
            JSONParser parser = new JSONParser();

            try (FileReader reader = new FileReader(jsonUsersFile)) {
                Object obj = parser.parse(reader);
                JSONArray usersListFromJson = (JSONArray) obj;

                for (Object userObject : usersListFromJson) {
                    JSONObject user = (JSONObject) userObject;
                    JSONObject userFromJson = (JSONObject) user.get("user");

                    String firstNameFromJson = (String) userFromJson.get("firstName");
                    String lastNameFromJson = (String) userFromJson.get("lastName");
                    String loginFromJson = (String) userFromJson.get("login");
                    String passwordFromJson = (String) userFromJson.get("password");

                    if (loginFromJson.equals(clientName)) {
                        passwordFromJson = newPassword;
                        User user1 = new User(firstNameFromJson, lastNameFromJson, loginFromJson, passwordFromJson);
                        user1.saveToFile();
                    }
                }
                sendToClient("haslo zmienione");

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private static void help() throws IOException {
        sendToClient("{\"addUser\" : \"dodaje nowego uzytkownika\", " +
                "\"login\" : \"umozliwia zalogowanie uzytkownika\", " +
                "\"uptime\" : \"zwraca czas zycia serwera\", " +
                "\"info\" : \"zwraca numer wersji serwera, date jego utworzenia\", " +
                "\"help\" : \"zwraca liste dostepnych komend\", " +
                "\"stop\" : \"zatrzymuje jednoczesnie serwer i klienta\"}");
    }

    private static void login() throws IOException, ParseException {
        String login = bufferedReader.readLine();
        String password = bufferedReader.readLine();

        JSONParser parser = new JSONParser();

        try (FileReader reader = new FileReader(jsonUsersFile)) {
            Object obj = parser.parse(reader);
            JSONArray usersListFromJson = (JSONArray) obj;

            boolean isValid = false;

            for (Object userObject : usersListFromJson) {
                JSONObject user = (JSONObject) userObject;
                JSONObject userFromJson = (JSONObject) user.get("user");

                String loginFromJson = (String) userFromJson.get("login");
                String passwordFromJson = (String) userFromJson.get("password");

                if (loginFromJson.equals(login) && passwordFromJson.equals(password)) {
                    isValid = true;
                }
            }

            if (isValid) {
                clientName = login;
                sendToClient("uzytkownik zalogowany");
            } else {
                sendToClient("uzytkownik niezalogowany");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static void sendToClient(String message) throws IOException {
        printWriter.println(message);
        Message msg = new Message(message, "server");
        msg.saveToFile();
    }

    private static void addUser() throws IOException, ParseException {

        String firstName = bufferedReader.readLine();
        String lastName = bufferedReader.readLine();
        String login = bufferedReader.readLine();
        String password = bufferedReader.readLine();

        User user = new User(firstName, lastName, login, password);
        user.saveToFile();
        sendToClient("Uzytkownik dodany");
    }

    private static void uptimeServer() throws IOException {
        LocalDateTime now = LocalDateTime.now();
        Duration timeElapsed = Duration.between(startTime, now);
        int days = Integer.parseInt(String.valueOf(timeElapsed.toDays()));
        int hours = Integer.parseInt(String.valueOf(timeElapsed.toHours()));
        int minutes = Integer.parseInt(String.valueOf(timeElapsed.toMinutes()));
        int seconds = Integer.parseInt(String.valueOf(timeElapsed.toSeconds()));
        String uptime = "{\"czas zycia\" : {\"dni\" : " + days + ", \"godziny\" : " + hours % 24 + ", \"minuty\" : " + minutes % 60 + ", \"sekundy\" : " + seconds % 60 + "}";
        sendToClient(uptime);
    }

    private static void infoServer() throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy 'at' hh:mm a");
        String info = "{\"serwer\" : {\"wersja\" : " + version + ", \"data utworzenia\" : \"" + startTime.format(formatter) + "\"}}";
        sendToClient(info);
    }

    private static void stop() throws IOException {
        bufferedReader.close();
        printWriter.close();
        socket.close();
        serverSocket.close();
        System.out.println("Serwer zostal zatrzymany");
        System.exit(0);
    }
}

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class User {

    private String firstName;
    private String lastName;
    private String login;
    private String password;
    private File jsonUsersFile = new File("users.json");
    private static JSONArray usersListToJson = new JSONArray();

    public User(String firstName, String lastName, String login, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.password = password;
    }



    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public Map toMap() {
        Map<String, String> usersMap = new HashMap<>();
        usersMap.put("firstName", firstName);
        usersMap.put("lastName", lastName);
        usersMap.put("login", login);
        usersMap.put("password", password);
        return usersMap;
    }

    public void saveToFile() throws IOException {
        JSONObject jsonUserObj = new JSONObject();
        jsonUserObj.put("user", this.toMap());
        usersListToJson.add(jsonUserObj);
        FileWriter usersFile = new FileWriter(jsonUsersFile);
        JsonUserFile jsonFile = new JsonUserFile(usersListToJson, usersFile);
        jsonFile.saveToJsonFile();
    }


}

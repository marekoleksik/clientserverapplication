import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private static Scanner scan = new Scanner(System.in);
    private static Socket socket;
    private static PrintWriter printWriter;
    private static BufferedReader bufferedReader;
    private static JSONObject main = new JSONObject();

    public static void main(String[] args) throws IOException, ParseException {
        socket = new Socket("localhost", 4999);
        printWriter = new PrintWriter(socket.getOutputStream(), true);
        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        System.out.println("Wpisz polecenie help, aby zobaczyc dostepne polecenia.");
        String input;

        while ((input = scan.next()) != null) {
            switch (input) {
                case "addUser":
                    addUser();
                    break;
                case "changePass":
                    changePass();
                    break;
                case "login":
                    login();
                    break;
                case "uptime":
                    sendToServer("uptime");
                    break;
                case "info":
                    sendToServer("info");
                    break;
                case "help":
                    sendToServer("help");
                    break;
                case "stop":
                    sendToServer("stop");
                    stop();
                    break;
                default:
                    sendToServer("badSyntax");
                    break;
            }

            String str = bufferedReader.readLine();
            System.out.println("server: " + str);
        }
    }

    private static void changePass() {
        sendToServer("changePass");
        System.out.println("Wpisz nowe hasło: ");
        String newPassword = scan.next();
        sendToServer(newPassword);
    }

    private static void login() throws IOException, ParseException {
        sendToServer("login");
        System.out.println("Wpisz login: ");
        String login = scan.next();
        sendToServer(login);
        System.out.println("Wpisz hasło: ");
        String password = scan.next();
        sendToServer(password);
    }

    private static void addUser() {
        sendToServer("user");
        System.out.println("Podaj imie: ");
        String firstName = scan.next();
        sendToServer(firstName);
        System.out.println("Podaj nazwisko: ");
        String lastName = scan.next();
        sendToServer(lastName);
        System.out.println("Podaj login: ");
        String login = scan.next();
        sendToServer(login);
        System.out.println("Podaj haslo: ");
        String password = scan.next();
        sendToServer(password);
    }

    private static void stop() throws IOException {
        printWriter.close();
        bufferedReader.close();
        socket.close();
        System.out.println("Klient zostal zatrzymany");
        System.exit(0);
    }

    private static void sendToServer(String input) {
        printWriter.println(input);
    }
}

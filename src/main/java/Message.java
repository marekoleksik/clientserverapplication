import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Message {
    private String message;
    private String sender;
    private static JSONArray messagesListToJson = new JSONArray();
    private File jsonMessagesFile;

    public Message(String message, String sender) {
        this.message = message;
        this.sender = sender;
    }

    public void saveToFile() throws IOException {
        jsonMessagesFile = new File("messages.json");
        JSONObject jsonUserObj = new JSONObject();
        jsonUserObj.put(sender, message);
        messagesListToJson.add(jsonUserObj);
        FileWriter messagesFile = new FileWriter(jsonMessagesFile);
        JsonFile jsonFile = new JsonFile(messagesListToJson, messagesFile);
        jsonFile.saveToJsonFile();
    }
}

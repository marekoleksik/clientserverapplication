import org.json.simple.JSONArray;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class JsonFile {

    private JSONArray listToJson;
    private FileWriter file;

    public JsonFile(JSONArray listToJson, FileWriter file) {
        this.listToJson = listToJson;
        this.file = file;
    }

    public void saveToJsonFile() {
        try {
            BufferedWriter out = new BufferedWriter(file);
            out.write(String.valueOf(listToJson.toJSONString()));
            out.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
